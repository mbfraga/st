#!/bin/sh

function _exit () {
   printf "[failed]\n"
   echo "exiting..."
   exit 1
}
function _ok () {
   printf "[OK]\n"
}

patches="./patches"

echo "$patches"

printf "Applying clipboard patch..."
git apply "$patches/st-clipboard-20180309-c5ba9c0.diff" & _ok || _exit

printf "Applying scrollback patch..."
git apply "$patches/st-scrollback-0.8.diff" && _ok || _exit

printf "Applying scrollback-mouse patch..."
git apply "$patches/st-scrollback-mouse-0.8.diff" && _ok || _exit

printf "Applying scrollback-mouse_altscreen patch..."
git apply "$patches/st-scrollback-mouse-altscreen-0.8.diff" && _ok || _exit

printf "Applying scrollback generic altscreen..."
git apply "$patches/st-scrollback-generic_altscreen-20181104-6f0f2b7.diff" && _ok || _exit

printf "Applying spoiler patch..."
git apply "$patches/st-spoiler-20180309-c5ba9c0.diff" && _ok || _exit

printf "Applying no_bold_colors patch..."
git apply "$patches/st-no_bold_colors-20170623-b331da5.diff" && _ok || _exit

printf "Applying externalpipe and specchar patch..."
git apply "$patches/st-externalpipe-specchar-20181104-6f0f2b7.diff" && _ok || _exit

printf "Apply 255 colors fix patch..."
git apply "$patches/st-255colors-20171102-0ac685f.diff" && _ok || _exit

