wget https://st.suckless.org/patches/clipboard/st-clipboard-20180309-c5ba9c0.diff
wget https://st.suckless.org/patches/externalpipe/st-externalpipe-20181016-c19e14b.diff
wget https://st.suckless.org/patches/iso14755/st-iso14755-20180911-67d0cb6.diff
wget https://st.suckless.org/patches/scrollback/st-scrollback-0.8.diff
wget https://st.suckless.org/patches/scrollback/st-scrollback-mouse-0.8.diff
wget https://st.suckless.org/patches/scrollback/st-scrollback-mouse-altscreen-0.8.diff
wget https://st.suckless.org/patches/solarized/st-no_bold_colors-20170623-b331da5.diff
wget https://st.suckless.org/patches/spoiler/st-spoiler-20180309-c5ba9c0.diff

